base   gitlabtags
author Joerg Hampel
email  joerg@hampel.at
date   2015-01-11
name   Gitlab API for Tags
desc   Shows tags from gitlab.com for a given project and user API token
url    https://gitlab.com/joerg.hampel/dokuwiki-gitlabapi/
